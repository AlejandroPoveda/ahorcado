class Encarcelado{

    //Atributos
    _palabra;
    _letra;

    //Metodo constructor de ejecuta cuando se crea un objeto
    constructor(valor){
        this._palabra=valor;
    }

    set palabra(valor){
        this._palabra=valor;
    }

    iniciarJuego(){
        //Verificar el tamaño de la palabra
        let a=this._palabra.length;
        let formulario=document.getElementById("formBotones");
        
        for(let i=0;i<a;i++){
            //Crear botones
            let miBoton
            miBoton=document.createElement("input");
            miBoton.setAttribute("type","button");
            miBoton.setAttribute("id","boton"+i);
            miBoton.setAttribute("value"," "); 
            formulario.appendChild(miBoton);
        }
    }
    buscarLetra(){
        this._letra=document.getElementById("letra").value;
        let n;
        //alert(n);
        let a=this._palabra.length;
        for(let i=0;i<a;i++){
            n = this._palabra.indexOf(this._letra,i);
            if (n>=0){
                document.getElementById("boton"+n).value=this._letra;
            }
        }
        if(this._palabra.indexOf(this._letra)<0){
            alert("No esta");
        }
        document.getElementById("letra").value="";
    }
}
let miJuego=new Encarcelado("cualquiera");
miJuego.iniciarJuego();
